package com.company;

class Circle {
    private double radius;
    private double ference;
    private double area;
    final static double PI = 3.14;

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return area;
    }

    public double getFerence() {
        return ference;
    }

    public void setRadius(double radius) {
        this.radius = Math.abs(radius);
        this.ference = 2 * PI * radius;
        this.area = radius * radius * PI;
    }


    public void setFerence(double ference) {
        this.ference = Math.abs(ference);
        this.radius = ference / (2 * PI);
        this.area = ference * ference / (4 * PI);
    }

    public void setArea(double area) {
        this.area = Math.abs(area);
        this.radius = Math.sqrt((area / PI));
        this.ference = 2 * Math.sqrt(this.area * PI);

    }


    public String toString() {
        return "радиус: " + radius + " длина: " + ference + " площадь: " + area;
    }

}

public class lab1_6 {
    public static void main(String[] args) {
        double oneMeterFenceCost = 2000;
        double oneMeterTrackCost = 1000;
        double fenceCost;
        double trackCost;
        Circle pull = new Circle();
        Circle fence = new Circle();
        pull.setRadius(3);
        fence.setRadius(pull.getRadius() + 1);
        fenceCost = fence.getFerence() * oneMeterFenceCost;
        trackCost = (fence.getArea() - pull.getArea()) * oneMeterTrackCost;
        System.out.println("Решение задачи 'бассейн'");
        System.out.println("Стоимость ограды: " + fenceCost + " руб. " + "\n" + "Стоимость дорожки: " + trackCost + " руб."+"\n");
        Circle earth = new Circle();
        Circle rope = new Circle();
        earth.setRadius(6378.1);
        rope.setFerence(earth.getFerence()+0.001);
        double gap = rope.getRadius()-earth.getRadius();
        double gapMeters = gap*1000;
        System.out.println("Решение задачи 'Земля и веревка'");
        System.out.printf("Зазор между Землей и веревкой составляет: " + "%.2f%s",gapMeters, " метров");

    }
}