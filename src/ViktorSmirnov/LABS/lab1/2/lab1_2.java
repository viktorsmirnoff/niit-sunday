package com.company;
import java.util.Scanner;


class Sqrt {
    double delta;
    double arg;

    Sqrt(double arg, double delta) {
        this.arg = arg;
        this.delta = delta;
    }

    double average(double x, double y) {
        return (x + y) / 2.0;
    }

    boolean good(double guess, double x) {
        return Math.abs(guess * guess - x) < delta;
    }

    double improve(double guess, double x) {
        return average(guess, (x / guess));
    }

    double iter(double guess, double x) {
        if (good(guess, x))
            return guess;
        else
            return iter(improve(guess, x), x);
    }

    public double calc() {
        return iter(1.0, arg);
    }
}

public class lab1_2 {
    public static void main(String[] args) {
        Scanner number = new Scanner(System.in);
        Scanner delta = new Scanner(System.in);
        System.out.println("Введите число, из которого хотите извлечь квадратный корень: ");
        String s = number.nextLine();
        System.out.println("Введите требуемую точность расчетов:");
        String d = delta.nextLine();
        double val = Double.parseDouble(String.valueOf(s));
        double val1 = Double.parseDouble(String.valueOf(d));
        Sqrt sqrt = new Sqrt(val, val1);
        double result = sqrt.calc();
        System.out.println("Sqrt of " + val + "=" + result);
    }
}