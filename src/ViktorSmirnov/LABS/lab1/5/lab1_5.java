package com.company;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Лабораторная №1, задача №5  Вывод числа символами
 */
public class lab1_5 {
    private static String[] array0 = {
            "    ***    ",
            "  *     *  ",
            " *       * ",
            " *       * ",
            " *       * ",
            " *       * ",
            "  *     *  ",
            "    ***    "
    };
    private static String[] array1 = {
            "     *     ",
            "    **     ",
            "   * *     ",
            "     *     ",
            "     *     ",
            "     *     ",
            "     *     ",
            "    ***    "
    };
    private static String[] array2 = {
            "   ***    ",
            " *     *  ",
            " *      * ",
            "       *  ",
            "      *   ",
            "    *     ",
            "  *       ",
            " ******** "
    };
    private static String[] array3 = {
            " ******** ",
            "        * ",
            "        * ",
            "   ****** ",
            "        * ",
            "        * ",
            "        * ",
            " ******** "
    };
    private static String[] array4 = {
            "  *     * ",
            "  *     * ",
            "  *     * ",
            "  ******* ",
            "        * ",
            "        * ",
            "        * ",
            "        * "
    };
    private static String[] array5 = {
            "  *******  ",
            " *         ",
            " *         ",
            "  *******  ",
            "         * ",
            "         * ",
            "         * ",
            "  *******  "
    };
    private static String[] array6 = {
            "  *******  ",
            " *         ",
            " *         ",
            " ********  ",
            " *       * ",
            " *       * ",
            " *       * ",
            "  *******  "
    };
    private static String[] array7 = {
            " ********* ",
            "        *  ",
            "       *   ",
            "      *    ",
            "     *     ",
            "    *      ",
            "   *       ",
            "  *        "
    };
    private static String[] array8 = {
            "  *******  ",
            " *       * ",
            " *       * ",
            " ********* ",
            " *       * ",
            " *       * ",
            " *       * ",
            "  *******  "
    };
    private static String[] array9 = {
            "  *******  ",
            " *       * ",
            " *       * ",
            "  ******** ",
            "         * ",
            "        *  ",
            "       *   ",
            "      *    "
    };


    private static String function(int integer, char array[]) {
        String str = "";

        for (int i = 0; i < array.length; i++) {
            if (array[i] == '0') {
                str += array0[integer];
            } else if (array[i] == '1') {
                str += array1[integer];
            } else if (array[i] == '2') {
                str += array2[integer];
            } else if (array[i] == '3') {
                str += array3[integer];
            } else if (array[i] == '4') {
                str += array4[integer];
            } else if (array[i] == '5') {
                str += array5[integer];
            } else if (array[i] == '6') {
                str += array6[integer];
            } else if (array[i] == '7') {
                str += array7[integer];
            } else if (array[i] == '8') {
                str += array8[integer];
            } else if (array[i] == '9') {
                str += array9[integer];
            }
        }

        return str;
    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите числа, которые хотите представить в виде символов: ");
        String s = scan.nextLine();
        char[] e = s.toCharArray();
        String[] stroka = new String[8];
        for (int i = 0; i < 8; i++) {
            stroka[i] = function(i, e);
            System.out.println(stroka[i]);
        }

    }
}