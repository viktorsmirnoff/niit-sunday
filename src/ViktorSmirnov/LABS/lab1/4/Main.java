package com.company;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        String finalString = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите последовательность для свертки в формате 1,2,3,8,9,10,11,12,13,20,21... ");
        String string = scan.nextLine();
        String[] newString = string.split(",");
        int[] intArr = new int[newString.length];
        int firstParam = Integer.parseInt(newString[0]);

        for (int i = 0; i < newString.length; i++) {
            intArr[i] = Integer.parseInt(newString[i]);
        }
        for (int i = 0; i < intArr.length; i++) {
            if (intArr.length == i + 1) {
                if (intArr[i] - firstParam == 1) {
                    finalString += firstParam + ", " + Integer.toString(intArr[i]);
                    continue;
                }
                finalString += stringClose(firstParam, intArr[i]);
                break;
            }
            if (intArr[i] == intArr[i + 1] - 1) {
                continue;
            }
            if (intArr[i] - firstParam == 1) {
                finalString += firstParam + ", " + Integer.toString(intArr[i]) + ", ";
                firstParam = intArr[i + 1];
                continue;
            }
            finalString += stringClose(firstParam, intArr[i]) + ", ";
            firstParam = intArr[i + 1];
        }

        System.out.println("'" + string + "'" + " -> " + finalString.substring(0, finalString.length()) + ".");
    }

    static String stringClose(int a, int b) {
        String symb1 = Integer.toString(a);
        String symb2 = Integer.toString(b);
        return a == b ? symb1 : symb1 + "-" + symb2;
    }

}