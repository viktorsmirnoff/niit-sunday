package com.company;

import java.util.Scanner;

/**
 * Лабораторная №1, задание №3  Развертка числовых диапазонов
 */
public class lab1_3 {
    public static void main(String[] args) {


        String finalString = "";
        Scanner scan = new Scanner(System.in);
        System.out.print("Введите последовательность из числовых диапазонов в формате 1,2,3-8,12,13-20,21... ");
        String string = scan.nextLine();
        String[] stringOfArrays = string.split(",");
        for (int i = 0; i < stringOfArrays.length; i++) {
            boolean isContain = stringOfArrays[i].contains("-");
            if (isContain) {
                stringOfArrays[i] = stringOpen(stringOfArrays[i]);
            }
        }
        for (int i = 0; i < stringOfArrays.length; i++) {
            finalString += stringOfArrays[i] + ",";

        }
        System.out.println("'" + string + "'" + "-> " + finalString.substring(0, finalString.length() - 1) + ".");
    }



    static String stringOpen(String str) {
        String newString = "";
        String[] newDestString = str.split("-");
        int a = Integer.parseInt(newDestString[0]);
        int b = Integer.parseInt(newDestString[1]);
        for (int i = a; i <= b; i++) {
            newString += i + ",";
        }
        return newString.substring(0, newString.length() - 1);
    }
}
