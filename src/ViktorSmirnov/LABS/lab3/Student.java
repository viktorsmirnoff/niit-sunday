package lab3;

/**
 * ������������ �3
 * <p>
 * ������ � ��������� ����� ��������� � ����� � ���� ������
 * �� ���������� �������:  "170000_1_0_������ �.�._4,4,4,4,4" .
 * ��������� ����� split("_") ������ �� ������ ������ �����, � �������:
 * [0]-������� ��� ������ ��������
 * [1]-������� ��� ����������������� ����� ������
 * [2]-������� ��� ���������, �������� �� ������� ���������(0-���, 1-��)
 * [3]-������� ��� �������.���.��������
 * [4]-������� ��� ������ ������
 * ���������� ������ ���������� �����(�������� �� ������� ��������)
 * ���������� ������ ���� �������� ���������� �������
 */
public class Student {


    private final int ID;
    private final String FIO;
    int groupID;
    Group groupReference;
    String Marks = "";
    boolean head = false;

    //�����������
    public Student(String FIO, int ID) {
        if (FIO.length()<2 && Integer.toString(ID).length() < 2) {
            throw new IllegalArgumentException();
        }
        this.FIO = FIO;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public String getFIO() {
        return FIO;
    }


    //���������� �������� � ������
    public void addToGroup(Group group) {
        this.groupID = group.getGroupID();
        this.groupReference = group;
        group.studentsReferences.add(this);
    }

    //���������� ������
    public void addMark(int mark) {
        if (numberOfMarks() < 5) {
            if (mark == 3 || mark == 4 || mark == 5)
                if (Marks.length() == 0) Marks += Integer.toString(mark);
                else Marks += ("," + Integer.toString(mark));
            else System.out.println("������� ������������ ������!");
        } else System.out.println("������� ��� ������� ��� ������");

    }

    //���������� ������
    public int numberOfMarks() {
        String[] numOfMarks = Marks.split(",");
        return numOfMarks.length;
    }

    //���������� ������� ������
    public double calcAverageMark() {
        double elementsSum = 0;
        if(numberOfMarks()<2){
            System.out.println("������������ ������ ��� ��������");
            return 0;
        }
        else {
            String[] marksToStringArray = Marks.split(",");
            int[] marksToIntArray = new int[marksToStringArray.length];
            for (int i = 0; i < marksToIntArray.length; i++) {
                marksToIntArray[i] = Integer.parseInt(marksToStringArray[i]);
                elementsSum += marksToIntArray[i];
            }
            return elementsSum / marksToIntArray.length;
        }

    }


    @Override
    public String toString() {
        if (head) {
            return FIO + " -> ������: " + "<< " + groupReference.getGroupNAME() + " >>" + ", �������� ��������� " + ", ������� ����: " + String.format("%.1f",calcAverageMark())+ ", ������: " + Marks;
        } else if(groupReference != null){
            return  FIO + " -> ������: " +"<< "+ groupReference.getGroupNAME() +" >>"+ ", ������� ����: " + String.format("%.1f",calcAverageMark())+ "; ������: " + Marks;
        }else {
            return FIO + " -> " + " � �������� �������� � ������ ������" + ", ������� ����: " + String.format("%.1f",calcAverageMark())+ "; ������: " + Marks;
        }
    }

}