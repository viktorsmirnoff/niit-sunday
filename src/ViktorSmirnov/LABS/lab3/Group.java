package lab3;


import java.util.ArrayList;

/**
 * ������������ �3
 * <p>
 * ������ � ������� ����� ��������� � ����� � ���� ������
 * �� ���������� �������: "�����_2_170000_170001,170002..."
 * ��������� ����� split("_") ������ �� ������ ������ �����, � �������:
 * [0]-��� �������� ������
 * [1]-��� ������������� ������
 * [2]-��� ������������� ��������. ���� �������� ���, �� ����� �������� 000000
 * [3]-��� ������ �� �������� ���������
 */


public class Group {

    private final int groupID;
    private final String groupNAME;
    ArrayList<Student> studentsReferences = new ArrayList<>();
    Student headReference;
    int headID;

    //�����������
    public Group(String groupNAME, int groupID) {
        this.groupNAME = groupNAME;
        this.groupID = groupID;
    }

    public int getGroupID() {
        return groupID;
    }

    public String getGroupNAME() {
        return groupNAME;
    }


    //���������� �������� � ������
    public void addStudent(Student student) {
        studentsReferences.add(student);
        student.groupReference = this;
        student.groupID = this.getGroupID();
    }


    //���������� ��������
    public void headAppointment(Student student) {
        if (this.searchStudentByID(student.getID())== null) {
            System.out.println("������� �������� � ������ ������ � �� ����� ���� ��������� ����!");
        } else if (student.numberOfMarks() < 5) {
            System.out.println("� �������� �� ����� ���� ���������� ���������!");
        } else if (headReference != null) {
            headReference.head = false;
            headReference = student;
            student.head = true;
            this.headID = student.getID();
        } else {
            headReference = student;
            student.head = true;
            this.headID = student.getID();
        }
    }

    //����� ��������
    public Student headSearch() {
        return headReference;
    }

    //����� �������� �� ��
    public Student searchStudentByID(int studentID) {
        if (Integer.toString(studentID).length() < 6) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < studentsReferences.size(); i++)
            if (studentID == (studentsReferences.get(i)).getID()) {
                return studentsReferences.get(i);
            }
        return null;
    }

    // ����� �������� �� ���
    public Student searchStudentByFIO(String studentFIO) {
        if (studentFIO.equals("")) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < studentsReferences.size(); i++) {
            if (studentFIO.equals((studentsReferences.get(i)).getFIO())) {
                return studentsReferences.get(i);
            }
        }
        return null;
    }

    //���������� �������� ����� � ������
    public double calcAverageMarkInGroup() {
        double marksSum = 0;
        for (int i = 0; i < studentsReferences.size(); i++) {
            marksSum += studentsReferences.get(i).calcAverageMark();
        }
        return marksSum / studentsReferences.size();
    }

    //���������� ���������� ������ �� ������
    /*����� ��� �������� ����������*/
    public int calcAmountOfMarksInGroup() {
        int amountOfMarks = 0;
        for (int i = 0; i < studentsReferences.size(); i++) {
            amountOfMarks += studentsReferences.get(i).numberOfMarks();
        }
        return amountOfMarks;
    }

    //���������� ����������� ���������� ���������� ������ �� ������
    /*����� ��� �������� ����������*/
    public int calcMaxAmountOfMarksInGroup() {
        return studentsReferences.size() * 5;
    }

    //���������� �������� �� ������
    public void expulsionFromGroup(Student student) {
        if (this.searchStudentByID(student.getID())!=null) {
            this.studentsReferences.remove(student);
            student.groupReference = null;
            student.groupID = 0;
        } else {
            System.out.println("��������� ������� �� ������! ��������� ������������ ��������� ������");
        }
    }

    @Override
    public String toString() {
        if(headReference == null){
            return "������ " + "<< "+groupNAME +" >> "+ "ID: "+ getGroupID() +" �������� ������: �� ��������"+", ���������� �������� � ������: "+ Integer.toString(studentsReferences.size());
        }
        return "������ " +"<< "+ groupNAME +" >> "+ "ID: "+ getGroupID() +" �������� ������: "+headReference.getFIO()+", ���������� �������� � ������: "+ Integer.toString(studentsReferences.size());
    }
}