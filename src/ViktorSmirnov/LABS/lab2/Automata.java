import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.*;
import java.util.ArrayList;


public class Automata {
    private STATES state ;
    private double cash = 0;
    private double needMoney ;
    private String yourDrinkName = "";

    ArrayList<Drink> drinks = new ArrayList<>();

    enum STATES {
        on(" �������!... "),
        off(" ��������! �������� �������... "),
        waitingChoice(" �������� ������... "),
        cooking(" ������������� �������... "),
        giveOddMoney(" ����� ������... "),
        giveDrink(" ����� �������... "),
        cashNow(" �������� �� ������� ������...");
        final String string;

        STATES(String string) {
            this.string = string;
        }

        public String PrintState() {
            return string;
        }

    }

    //����� "�������"
    class Drink {
        private String name;
        private double price;

        public Drink(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }
    }

    //��������������� ������
    private double getCash() {
        return cash;
    }

    private void setCash(double cash) {
        this.cash = cash;
    }

    private double getNeedMoney() {
        return needMoney;
    }

    private String getYourDrinkName() {
        return yourDrinkName;
    }

    public void setNeedMoney(double needMoney) {
        this.needMoney = needMoney;
    }

    private void setYourDrinkName(String yourDrinkName) {
        this.yourDrinkName = yourDrinkName;
    }


    //�������� ������
    public void On()  {
        this.state = STATES.on;
        state.PrintState();
    }

    public void Off() {
        this.state = STATES.off;
        state.PrintState();
    }

    //�������� ���� ��������
    private void addDrinksFromFile(String filename) {
        try {
            FileReader file = new FileReader(filename);
            JSONParser parser = new JSONParser();
            Object parseObject = parser.parse(file);
            JSONObject jsObject = (JSONObject) parseObject;
            JSONArray drinksArray = (JSONArray) jsObject.get("drinks");
            for (Object i : drinksArray) {
                Drink drink = new Drink(
                        (((JSONObject) i).get("name")).toString(),
                        Double.parseDouble((((JSONObject) i).get("price")).toString()));

                drinks.add(drink);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    public void buildMenu(){
        this.state = STATES.waitingChoice;
        state.PrintState();
        if (drinks.size()==0) System.out.println("������� �� ����������");
        else {
            String drinksChoise = "��� ������ ������� ������� ";
            for (int i = 0; i < drinks.size(); i++) {
                System.out.println((i + 1) + ")" + drinks.get(i).getName() + " ���������: " + drinks.get(i).getPrice() + " ���.");
                drinksChoise += i + 1 + ", ";
            }
            System.out.println(drinksChoise);
        }
    }

    public void addCoins(double coin) {
        this.cash += coin;
    }

    public boolean checkCash() {
        this.state = STATES.cashNow;
        state.PrintState();
        if (cash >= needMoney && cash!=0) return true;
        else return false;
    }

    public void chooseDrink(int drinkNumber) {
        if(drinkNumber>drinks.size()) System.out.println("������� � ����� ������� �� ���������");
        else {
            yourDrinkName = drinks.get(drinkNumber-1).getName();
            setNeedMoney(drinks.get(drinkNumber-1).getPrice());
        }
    }

    public void Cook() {
        this.state = STATES.cooking;
        state.PrintState();
        if(yourDrinkName.equals("")) System.out.println("������� �� ������");
        else if (!checkCash()) System.out.println(
                "������������ ��������, ���������� ��� "+ Double.toString(getNeedMoney()-getCash())+" ���.");
        else {
            cash -= needMoney;
            for (int i = 0; i < 4; i++) {
                System.out.println("Cook!");
            }
            takeDrink();
        }
    }

    private String takeDrink() {
        this.state = STATES.giveDrink;
        state.PrintState();
        return "��� ������� "+ getYourDrinkName();
    }

    public String ReturnMoney() {
        this.state = STATES.giveOddMoney;
        state.PrintState();
        String s = String.format("�������� ������: " + "%.2f%s", getCash(), " ���.");
        setCash(0);
        return s;
    }



    //    ����������� Automata
    public Automata(String drinksFileName) {
        addDrinksFromFile(drinksFileName);
    }


}